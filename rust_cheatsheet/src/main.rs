/*------------------TOOLCHAIN INSTALLER----------------------
rustup update

--------------------CARGO SPECIFICS-----------------------------
cargo new x --bin   <-  creates a project called x
cargo check         <-  compiles
cargo build         <-  compile and creates executables
cargo run           <-  compiles and runs code
cargo update        <-  updates dependencies in .toml, but 
                        not major versions (do those manually)
cargo fix           <-  implements the compilers' suggestions
cargo doc --open    <-  opens crates' documentations in browser 
cargo clean         <-  fixes os 13 error
*/




//--------------------I/O--------------------------------------
fn print_ex(){
    println!("Hello, world!");
    let greetings = "Heya~"; //this is a string literal
    println!("{}", greetings);
}

fn main(){
    print_ex();
}


//--------------------DECLARING VARS----------------------------

let mut foo = 1; //creates mutable var
let mut foo = String::new(); //creates mutable string

let x = 5;
let x = 6; //shadow a variable, so it's only changed here, then immutable

let mut y = 5;  //or make it mutable forever, so no more 'let' is needed:
y = y+1;

let num:u32 = 42_000; //u can use _ as a seperator
//u32 means "unsigned 32bit integer". so it stores nums up to 2³²-1

let f32 = 0.3;
let f64 = 0.45;
//--------------------MATH----------------------------------------
let x = 5 /3;  //returns 1, it doesn't change to float

//--------------------STRINGS-------------------------------------
let c = 'z'; //single quoute creates a char; only one letter; emojis/chinese l. possible
let d = "zz"; //double qoute for strings

let mut food = String::from("Schoko");
//or empty:
//let mut food = String::new(); 
food.push_str("lade");
println!("{}", food);


//string slices, '..' meaning range
let s = String::from("hello world");
let hello = &s[0..5];
//iterate through a string and return string slice (str)
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();
    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }
    &s[..]
}
let word = first_word(&s);
//&str, here word, is an immutable reference 
//and are the same as string literals such as the above "greetings"


//----------------------LISTS-------------------------------------
let tup: (i32, f64, u8) = (500, 6.4, 1); //this is a tuple, it's ordered!

let (x, y, z) = tup; //you can safely deconstruct them like this or like so:
let five_hundred = tup.0;

let a_list = ["One","Two","Three"]; //allows only one data type and cannot change size
let first_item = a_list[0];


let a = [1, 2, 3, 4, 5];
let slice = &a[1..3]; //this slice has the type &[i32]


//-----------------------FUNCS-------------------------
fn call_me(text:&str, num:u32, condition:bool){
    println!("{} {} {}",text,num, condition);
}


//---------------------OWNERSHIP------------------------------
//every value has a variable, it's owner! only one owner per value!
//owner out of scope -> value dropped
{
    let num:u8 = 3;
    //here u can use num
}   //here it's dropped


let s1 = String::from("hello");
let s2 = s1; //here, s1 is invalidated. Since s1 was on the heap, 
//changing ownership kills the variable to ensure safe memory freeing l8er

let s1 = String::from("hello");
//use:
let s2 = s1.clone(); //instead, now you have an entirely new thing in RAM
//and s1 is still alive
let x = 5;     // x comes into scope
add_one(x);     //x has copy trait, so it's not invalidated!
               //if this was s2, it would be invalidated here           
println!("{}",x); //but it's not

fn add_one(my_int:i32)->i32{ 
    my_int+3//here you can use x/my_int
}

let name=String::from("Hans");    
let new_name = give_and_take(name);      
//println!("{}",name); //not possible after move, use new_name 
//ofc you can just give the same name

fn give_and_take(name:String)->String{
    name
}

//you can also pass and return multiple values with tups
//this can become tedious, so instead there are

//--------------------------REFERENCES&BORROWING---------------------------

let s1 = String::from("hello");
let len = calculate_length(&s1);
fn calculate_length(s: &String) -> usize { // s is a reference to a String
    s.len(
)} 
//here, the referenced value cannot be modified. if we add "mut", 
//exactly one thing at a time is allowed to change it

let mut s = String::from("hello");
{
    let r1 = &mut s;
} // r1 goes out of scope here, so we can make a new reference with no problems.
let r2 = &mut s;
//r1 and r2 cannot be in the same scope

/*At any given time, you can have either one mutable reference 
or any number of immutable references.
References must always be valid.*/

//----------------------------STRUCTS---------------------
//are like objects
struct BeerCase {
    brand: String,
    beer_count: u8,
    yummy: bool,
}

let mut krombacher = BeerCase { // entire instance is either mutable or not;
    brand: String::from("Krombacher"),
    beer_count:20,
    yummy: true,
};


krombacher.brand.push_str("_alkoholfrei");
krombacher.yummy=false;
//if the parameters match the keys, you can skip them in funcs:
//field init syntax
fn build_case(brand: String, yummy:bool) -> BeerCase {
    BeerCase {
        brand,
        yummy,
        beer_count: 20, //it's always 20, but we didn't have to assign
        //brand:brand and yummy:yummy
    }
}

let beer2 = build_case(String::from("Erdinger_Weißbier"), false);

//we can also shorten the creation of more instances by auto filling
//struct update syntax
let beer3 = BeerCase {
    brand: String::from("Becks_Ice"),
    ..beer2  //this will assign erdingers other values to becks ice
}; 


fn showcase_beer_case(beer_case:BeerCase){
    println!("This is {}", beer_case.brand);
    println!("Does it taste good? {}!", beer_case.yummy);
    println!("One case consist of {} bottles.", beer_case.beer_count);
}
showcase_beer_case(beer3);
/*prints:   This is Becks_Ice
            Does it taste good? false!
            One case consist of 20 bottles. */

//you can create structs without naming the attributes/fields like so:
//Tuple Structs:
struct Rgb(u8, u8,u8);
let red = Rgb(250,0,0); 

fn showcase_rgb(rgb_instance:&Rgb){
    println!("Red: {}", rgb_instance.0);
    println!("Green: {}", rgb_instance.1);
    println!("Blue: {}", rgb_instance.2);
}

showcase_rgb(&red);
//You can give structs the debug trait for printing it's fields
#[derive(Debug)]
struct BottleCap{
    height: f64,
    radius: u32,
    rarity: String
    }

let cola_cap = BottleCap{radius:3, height:0.12, rarity:String::from("common") };
println!("{:#?}",cola_cap);

//iterating through a vector of structs:
fn iter_and_print() {

    let mut bottle_collection: Vec<BottleCap> = Vec::new();
    
    let cola_cap = BottleCap{radius:3, height:0.12, rarity:String::from("common")};
    let vanilla_coke_cap = BottleCap{radius:4, height:0.12, rarity:String::from("uncommon")};
    
    for cap in bottle_collection.iter() {
        println!("{:?}", cap);
    }
}
iter_and_print();


//--------------------------------METHODS----------------------------------





//-------------------------------MODULES--------------------------------

//to use other files in same dir, do: 
mod otherfile
//then: 
otherfile::some_func();
//to use other files in lower dirs, do: 
use subdirname{anotherfile}




//--------------------------------GGEZ-------------------------------------
/*  Context: 
        all the sate required to interface with the hardware
    EventHandler:
        user implements to register callbacks für events, submodules: graphics/audio

Create a new Context object with default objects from a ContextBuilder or Conf object, 
and then call event::run() with the Context and an instance of your EventHandler 
to run your game's main loop


*/ 



//--------------------------------MISC--------------------------------
//if you get: error: could not execute process - do CARGO CLEAN
