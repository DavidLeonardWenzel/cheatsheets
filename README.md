# A cheatsheet collection. 

A wiser person once told me: when you learn something new, write it down. 
So here is the part of that collection that might be useful for others! 

So far, there are helpers of basic functionality for:  
    - Python  
    - Rust  
    - bash / terminal commands and shortcuts  
    - R (statistics)  
    - Sublime (Text Editor Shortcuts)  

In the future, I would like to add:   
    - JavaScript / node / npm  
    - React  
    - Svelte  
    - CSS  
    - Swift  
    - Kotlin  
    - DevOps 101, mainly docker  