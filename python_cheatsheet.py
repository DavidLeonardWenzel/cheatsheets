#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from __future__ import division
import time
import collections 
import logging
import os
import itertools
import numpy as np
import pandas as pd
import re
from nltk.corpus import wordnet

###################################### TOOLING INFO ##################################
#use pip install <modulename> to install dependencies
#use pip list to list modules
#use pip install -r requirements.txt to install all requirements if given in reqs.txt

dummy_string = u"""
    Ich schreibe bezüglich der Abgabe der Aufgaben im Seminar. 
    Ich habe aufgrund meiner Katze die Aufgaben komplett aus dem Auge verloren."""

dummy_list = [1,2,3,4,5,6,7,8,9,10]

dummy_dict ={"a":1, 
                "b":2,
                "c":3}
dummy_tuple=(1, 2)

###################################### NUMBER BEHAVIOUR ##################################
def division():
    division = 5 / 2 #2.5 <type 'float'>
    modulo = 10 % 3  #1 <type 'int'>, da Rest 1
    modulo2 = 6 % 2 #0 <type 'int'>, da kein Rest

    #Jetzt mit Floats selber: (anm: quotient und mit floats rechnen funktioniert unabhängig von import division)
    floatdivision=  1.25 / 0.5#2.5 <type 'float'>
    floatmodulo = 1.25 % 0.5 #0.25 <type 'float'>
    quotient = 28 // 3  #int 9, eig 9,3 aber es passt nur 9 mal rein 
    
    print(division, modulo, modulo2)
    print(floatdivision, floatmodulo, quotient)

def hoch():
    #2^5 / 2 hoch 5
    print(3**0) #0
    print(3**1) #3
    print(3**2) #9
    print(3**3) #27

def gerade_ungerade():
    #Gerade zahlen geben ints, ungerade floats bei :2?
    for zahl in dummy_list:
        ergebnis = zahl % 2
        print("bei", zahl, "ist Modulo 2 = ",ergebnis)
        print(type(ergebnis))
        #Gerade = 0 , ,ungerade = 1
     
def easymax():  
    highest = max(1, 2)
    print(highest)


################################## STRING BEHAVIOUR ########################################
def strings_and_literals():
    print("'Brian's'")
    print('Brian\'s')
    print("\nnewlines!")
    print("\\n is a literal slash n")
    print("data/pfad.txt")
    number = 9
    print("I'm a string with the number",number, "!")
    name = "Harald"
    print("The sheep is called"+name)
 

def cutting_strings():
    imastring = "String"
    print(imastring * 3)
    print(imastring[:3])#bis str
    laenge = len(imastring)
    print(type(laenge))
    print(imastring[:laenge])#komplett
    kuerzer = laenge-2 #bis stri
    print(imastring[:kuerzer])#bis stri
    print(imastring[:kuerzer-1]) #bis str

def affixe(word):
    wortformen = []
    laenge=len(word)
    while laenge >= 0:
        wortformen.append(word[:laenge])
        laenge-=1
    sorted_wortformen = wortformen[::-1]
    print(sorted_wortformen)  


def left_right_context(string, context_range):
    splitted_string= string.split()
    if context_range >= len(splitted_string):
        raise "Context longer than string!"
    for pos, current_token in enumerate(splitted_string):
        if pos-context_range < 0:
            left_context_token=None
            right_context_token = splitted_string[pos+context_range]
            print("Before:",left_context_token, "\nNow:",current_token,"\nAfter:",right_context_token, "\n")
        elif pos+context_range >= len(splitted_string):
            left_context_token = splitted_string[pos-context_range]
            right_context_token = None
            print("Before:",left_context_token, "\nNow:",current_token,"\nAfter:",right_context_token, "\n"           )
        else:
            left_context_token = splitted_string[pos-context_range]
            right_context_token = splitted_string[pos+context_range]
            print("Before:",left_context_token, "\nNow:",current_token,"\nAfter:",right_context_token, "\n")



#f strings
def string_formating():
    #Often we need variables in our print statements, and there the preferred way in py3.6 is to use so-called f-strings 
    #Put an f in your print statements to embed variables the fastest way
    x = "David"
    print(f"I am", {x})

    gold_winner, silver_winner, bronze_winner = "Peter", "Paul", "Marry"
    print(f"The gold winner is {gold_winner}, the silver winner is {silver_winner}, the bronze winner is {bronze_winner}")
    #you can also use  dict lookups instead of variables, like
    print(f"My fav. number is {dummy_dict['a']}") 

def f_string_floats():
    price = 92.23498214
    two_after_comma = f"This costs ${price:.02f}."
    print(two_after_comma)


def string_checks():
    print("milkachocolate".startswith("milka")) #returns True
    print("Milkachocolate".startswith("milka")) #returns False
    print("chocolate".startswith("m")) #returns False

def string_casing_manipulation():
    print("chocolate".upper()) #gives CHOCOLATE
    print("SCHOKOLADE".lower()) #gives schokolade

def substring_test():
    if "kola" in "schokolade":
        print("lecker kola schokolade")

def string_example_manipulation():
    wrong_noun= "schokoLade"
    correct_noun = wrong_noun[:1].upper() + wrong_noun[1:].lower()
    print(correct_noun) #gives Schokolade

def remove_whitespace():
    ugly_string1="oiajf "
    ugly_string2="oiajf \n"
    ugly_string3="oiajf \n "
    ugly_string4=" oiajf \n "
    ugly_string5="\n oiajf \n "
    stripped1 = ugly_string1.strip()
    stripped2 = ugly_string2.strip()
    stripped3 = ugly_string2.strip()
    stripped4 = ugly_string4.strip()
    stripped5 = ugly_string5.strip()
    rstripped1= ugly_string1.rstrip()
    rstripped2= ugly_string2.rstrip()
    rstripped3= ugly_string3.rstrip()
    rstripped4= ugly_string4.rstrip()
    rstripped5= ugly_string5.rstrip()

    print("the strings stripped are \n{a}\n{b}\n{c}\n{d}\n{e}".format(a=stripped1, b=stripped2, c=stripped3,d=stripped4, e=stripped5))
    print("the strings rstripped are \n{a}\n{b}\n{c}\n{d}\n{e}".format(a=rstripped1, b=rstripped2, c=rstripped3, d=rstripped4, e=rstripped5))
    
def conversion():
    zahl = 9
    print(type(zahl))
    zahl_jetzt= str(zahl)
    print(type(zahl_jetzt))

################################## LIST BEHAVIOUR ########################################
def reassign(mylist):
    mylist[2] = mylist[2]+1 #heißt: wir nehmen was an index stelle 2 steht, und an dieser stelle rechnen wir + 1
    print(mylist) #gives [1, 2, 4]

def about_in(mylist):
    if 2 in mylist:
        print("Yup, got it")
    if 3 not in mylist:
        print("i have no 3 :D") #doens't trigger, gives False
    if not 3 in mylist:
        print("alternative syntax") #doesn't trigger, gives False
    if "2" in mylist:
        print("and also as a string") #doesn't trigger
    if [1,2,3] in mylist:
        print("and as a list element") #doesn't trigger
    if mylist in [2]:
        print("works both ways") #doesn't trigger
 
def fast_list():
    tenthousand = range(10000)
    print(tenthousand) #0-9999 
    print(type(tenthousand)) #it's a list
    for _x in range(10):
        print("I ought to write 10 times: I will not waste print statements")
    print(sum(range(10))) #45, da 0+1+2+3+4+5+6+7+8+9=45

def pop(mylist):
    mylist.pop(0) #removes and returns the indexed (here: first) item
    mylist.pop() #without anything, removes and returns the last item
    #if our list contained 1,2 and 3, now only 2 is left
    print(mylist) #prints [2]

def reversing(mylist):
    rev = mylist[::-1] #gives 3,2,1
    print(rev)

def sorting():
    int_list = [3,325,2156]
    float_list = [3.12, 5.235, 3.01]
    string_list = ["zodiac", "hooray", "frolocking through the tulips", "saat", u"säher", u"ßcheibenkleister", u"ähnlich"]
    bool_list = [False, False, True, False, True, True, True]
    #sort/sorted
    print("sorted ints:",sorted(int_list))
    print("sorted floats:",sorted(float_list))
    print("sorted strings:",sorted(string_list)) #uses alphabet, puts umlaute last (i.e: saat before säher, ß-ä-o-ü last)
    print("sorted bools:",sorted(bool_list)) #puts False first


def sublist_sorting(index=2):
    #To sort a list by a specified nested part of it, you can sort by its sublist!
    sub_list =  [['Mission', 0, 3], ['Name', 1, 1], ['URL', 2, 4], ['Description', 3, 6], ['Creator Contact', 4, 10], ['Temporal Coverage', 5, 8], ['Publisher And Registrant', 6, 12], ['Credit Guidelines', 7, 13], ['Abstract', 8, 5]]
    # key is set to sort using the variable "index" as the position of sublist by which to sort 
    sub_list.sort(key = lambda x: x[index]) 
    return sub_list 

def proper_counting():
    #use Counter and keywithmaxvalue
    long_list = [1,1,1,2,3,4,5,6,6,6,6,6,7,8,8,8,8,6]
    as_sorted_dict = collections.Counter(long_list)
    print(as_sorted_dict) #gives Counter({6: 6, 8: 4, 1: 3, 2: 1, 3: 1, 4: 1, 5: 1, 7: 1})
    #know, to quickly get just the highest value there is a misc func included that does just that with the fol. syntax:
    highest = keywithmaxval(collections.Counter(long_list))
    print(highest) #is 6 (because it occurs 6 times, easymax would have given 8, the one with the highest value)
                    #this works on strings, floats and booleans as well

def permutations(list):
    #requires import itertools, returns list of sets of all possible 2-pairs
    #permutations/pairs/listpair
    return list(itertools.combinations(list, 2))


def list_comps():
    #L = [mapping-expression for element in source-list if filter-expression]
    #L2 = [a if C else b for i in items]
    a_list = [i * 3 for i in range(10)]
    print(a_list) #gives [0, 3, 6, 9, 12, 15, 18, 21, 24, 27]
    
    another_list = [i * 2 for i in a_list if i >= 10]
    print(another_list) #takes only  12, 15, 18, 21, 24, 27 from the last generated list, then doubles them to
                        #[24, 30, 36, 42, 48, 54]

    fizz_buzz = ["Fizz"*(not i%3) + "Buzz"*(not i%5) or i for i in range(1, 100)]
    print(fizz_buzz)

#map example (2 following funcs)
def greet(name):
    return "Hey %s" % name

guests = ["peter", "marry", "dave"]
def greet_each_guest(guests):
    return list(map(greet, guests))
    #map takes 2 args, a func and a list, and applies the func the each element


def filter_ex(list_ex):
    #removes list elements that are empty
    return filter(None, list_ex)

# The filter function will filter out every element of the iterable 
# for which the function passed to it is false

def filter_ex2():
    #array from 1 to 10
    a = range(1,11)
    print(list(filter(lambda x:x%2==0,a)))

#this iterates from 1 to 11. If the number is even (viz., if modulo 2 is true) 
#it'll be in the resulting list. if it is false, filter removes it. 
#the list is then printed:
#[2, 4, 6, 8, 10]


def regular_ex():
    """we have a list containing dirty and good strings and want only good ones"""
    dirty_list=["goody boy","hola","[removed]","perfect 5/7", "[removed]"]
    x=list(filter(lambda x: (x!="[removed]"), dirty_list))
    return x
    
def regex_filter_ex():
    """we have a list containing dirty and good strings and want only good ones
    the complexity of differentiating required a reg-ex"""
    character_re = re.compile('[a-zA-Z]')
    #this regex finds the good strings
    dirty_list = [ "AST2", "COSAC", "telemetry", "referred", "1.", "234325", "m", "---"]
    cleaned =list(filter(lambda x: re.search(character_re, x), dirty_list))

    return cleaned



############################### TUPLE ####################################################
#immutable, iterable, ordered
my_tuple = tuple()
another_tuple = ()
x = (1,1,2,2) #also a tuple
y = ("a",) #shorthand for singletons
z = "a", #evenshorterhand for singletons
various_types_tuple=("a", "b", 1, 2) #you can mix types!

def returns_mults():
    return 1,2
more_tuple = returns_mults() 

#the return type is also a tuple
#tuples implement all the common sequence operations (list-like!)

def list_like_commons(my_tuple):
    "a" in my_tuple #returns bool
    "b" not in my_tuple
    my_tuple[1] 
    len(my_tuple) 
    min(my_tuple) 
    my_tuple.count(1) #gives number of occurences of 1

    my_tuple + tuple("a")
    #you can use tuple1 + tuple2 to concatenate
    #but! carefull, they are immutable, so this actually
    #creates a new third tuple (you cannot change the original)
    #(it's usable but costly)


##################################### SET #################################################
#mutable, unordered, each item is unique (no duplicates, they just get squashed)
#fast intersection and other logical operations
my_set = set()
my_set.add("x")
my_set.add("x")
#still jut one x in there!


############################### DICTIONARY BEHAVIOUR ###########################################

def handling_dicts(mydict):
    print(mydict[3]) #returns c with our dummy, if it was an index this would be out of list error
    for entry in mydict:
        print(entry, type(entry)) #gives the KEYS, 1,2,3 as ints
    #this is done so that we can look for keys, and than get their value
        if entry == 3:
            print(mydict[entry]) #now we know, the third letter in the alphabet is a c :D
    #in case we know the value, we do:
    for value in mydict.values():
        print(value, type(value)) #thus we can basically invert dicts

    #to check for specific keys, we could either iterate through the whole as done above, or do this:
    try: 
        mydict[4]   #this throws an error, because it doesn't exist. we can tell python
                    #how to behave when encountering errors as seen here:
    except KeyError:
        print("We have no entry for 4, so lets create one")
        mydict[4] = "d"
        print(mydict) #now has 4

def get_value(mydict):
    this = mydict.get("hans", "not a num") + "..." 
    other = mydict.get(1) +"..."
    print(this, other)

def get_example_counting():
    a_dict=dict()

    dummy_list.append(1) #now, 1 occures twice
    for num in dummy_list:
        a_dict[num] = a_dict.get(num, 0)+1
    print(a_dict)


def killing_dicts(mydict):
    del mydict[2]   # remove entry with key '2'
    print("without 2:", mydict)

    mydict.clear()  # remove all entries in dict
    print(mydict)   # gives {}

    del mydict      # delete entire dictionary
    try: 
        print(mydict)
    except UnboundLocalError:
        print("Doesn't exist anymore, can't print it")

def keywithmaxval(mydict):
    #simple func that takes a dic, sorts it by key and returns the key of the highest value entry
    #usefull in conjunction with Counter, to immidiatly find out the most common entry in a list
    v=list(mydict.values())
    k=list(mydict.keys())
    #print k[v.index(max(v))]
    return k[v.index(max(v))] #maybe give the option to also give that value


def sort_dict_by_value(dict_in):
    """returns a list of tuples, where the first entries are the key,value-pairs
    with the highest numeric value"""
    return sorted(dict_in.items(), key=lambda x: x[1], reverse=True)


#now that you've seen most common types, here are important feature distinctions:

#The following are immutable:
    #int
    #float
    #decimal
    #bool
    #string
    #tuple

#The following are mutable objects:
    #list
    #dict
    #set
    #default on user-defined classes 


################################### WORKING WITH FILES ################################
#this version autmatically closes the file too and is most common and short:
def read(path):
    with open(path, 'r', encoding="utf-8") as f:
        return f.read()
        #return f.read().splitlines()

#to close the file explicitly, use:
def reading(path):
    my_f = open(path, "r", encoding="utf-8")
    f = my_f.read()
    my_f.close()
    print(f)

def write(path, content):
    my_f = open(path, "w", encoding="utf-8") #notice how r for <read> changed to w for <write>
    #if the path exists, it will overwrite all data, if not, create the file
    #my_f.write("I'm so excited to finally be in a textfile") 
    my_f.write(content)
    my_f.close()
    
    #note that this isn't restricted to txt files
    #you cannot write ints and floats, use str(int) or str(float) to be able to do that 

def careful_read(path):
    #this func does the same as reading but is a much more carefull and safe
    try:
        my_f=open(path,"r", "utf-8")
        try:
            f= my_f.read()
        except:  #happens when the file cannot be read, handle reading errors here
            print("Can't read this")
            raise #raises an error to manually stop the program
        finally:
            my_f.close()
    except IOError: #happens when the file doesn't exist, handle open errors heree
        print("Uh-Oh, no such file")
        raise #raises an error to manually stop the program


######################################OOP##############################

class story:
    def __init__(self, mycha, myknots, mystates):
        '''
        When called with the 3 args mycha, myknots and mystates, they
        will get assigned to this instance of class as 'class attributes' 
        '''
        self.cha= mycha 
        self.knots= myknots
        self.states=mystates
        #This attribute is always set to 'extreme', we do not need to specify it when creating the class
        self.awesomeness = "extreme"


    def test(self):
        self.chi= ["hallo"]
        print(self.chi)

    def bla(self):
        self.chi.append("du")
        print(self.chi)


    def choice(self, storypart):
        print(self.knots[storypart][0])
        outcomes=self.knots[storypart][1]
        options=self.knots[storypart][2]
        for num, option in enumerate(options):
            print("[{x}]:\t{y}".format(x=num,y=option))
        #print()
        usersaid=input("\n>>> Wie entscheidest du dich?\n")
        try:
            usersaid=int(usersaid)
        except ValueError:
            print("\nBitte eine Zahl angeben\n")
            return self.choice(storypart)
        try:
            accepted_choice = outcomes[usersaid]
        except IndexError:
            print("\nBitte eine der angegeben Zahlen angeben\n")
            return self.choice(storypart)

        print("Du hast dich entschieden: ",accepted_choice)
        story.chi = "chivalue"



def invoke_class():
    retriever=story("Pete", {}, [])
    print("Du bist", retriever.cha)
    print("Die Geschichte ist",retriever.awesomeness)

    intro=["Es war einmal eine fette Party",["hingehen", "absagen"], ["Ich gehe hin","Ich sage ab"]]
    hingehen=["Du feierst richtig ab",["No further options"]]
    absage = ["Du bleibst lieber zuhause",["No further options"]]

    retriever.knots["intro"]=intro
    retriever.knots["hingehen"]=hingehen
    retriever.knots["absage"]=absage

    story.choice(retriever, "intro")



####################################NUMPY####################################
#all of which require import numpy as np

def creating_arrays():
    my_list = [1,2,3,4]
    array = np.array(my_list)
    print(array)
    array_1 = np.array([[1,2,3],[9,8,7]])
    print(array_1)

def transposing():
    array_1 = np.array([[1,2,3],[9,8,7]])
    print(array_1)
    print(array_1.T)
    #gives:
    #[[1 2 3]
    #[9 8 7]]
    #
    #[[1 9]
    #[2 8]
    #[3 7]]



###############################EXCEPTIONS AND RAISE#####################

def asserting():
    #When we don't want our program to run any further under specific circumstances, we can use
    assert 2+2 == 4
    #with any boolean check. since this was true, the program will just run further
    assert 3 < 10
    #still going ...
    assert "lion".startswith("a")
    #it will stop here


def assert_ex():
    podBayDoorStatus="open"
    assert podBayDoorStatus == 'open', 'The pod bay doors need to be "open".'

    short="hm"
    longer="hmmmmmmm"
    assert len(short) == len(longer), "they should have the same length"


def named_exceptions():
    try: 
        raise Exception("hell")
    except Exception as err:
        if str(err) == "hell":
            print("ja")


################################LAMBDAS #################################
#lambdas are functions without names. learn a new syntax for functions, yay.
# type <lambda>, then the arguments, then <:>, then an expression
lambda name : "I am " + name
lambda bride, groom : bride + "will marry" + groom + "in August"

#to actually compute something, insert the arguments, as per usual, in brackets after the call.
res = (lambda name : "I am " + name)("Hans") 
#notice how i put the lambda in brackets. res is now "I am Hans"
#it's semantically easier to assign a name:
introduction = lambda name : "I am " + name
introduction("Pete") #results to "I am Pete". 
#So, why use this instead of functions?
#-> Lambdas are used when we quickly need slightly different functions.
#A regular function "foo" could take a function as an argument. 
#Every time we call "foo", we need to define the function we want to pass. Pass lambdas instead!

def foo(input_func):
    #do fancy stuff with the function here.
    input_func(10) #when called with the first lambda below, it's True, 
                    #called with the second lambda, it's False

foo(lambda x: (x%2==0))

foo(lambda x: (x%3==0))



def lambda_examples():
    print((lambda x:x+3)(7)) #10
    x = 10
    print((lambda x:x+3)(x)) #13


###############################GLOBAL BEHAVIOUR##########################
some_int = 0
def somefunc_reads():
    return some_int #we just "read" it and return it
    #0

#def somefunc_changes():
    #try:
        #some_int+= 1  # we change it
        #return some_int
    #except UnboundLocalError:
        #print("oh no! i dont know what that variable is")
#exception triggers because we cannot change a global var! only read, no write

def somefunc_imports_to_change():
    global some_int
    some_int+=1
    return some_int
    #works, is now globally and here 1



################################### EXAMPLES & UTILITY FUNCTIONS #################################
def is_it():
    if "2".isdigit():
        print("jo")

def recursive_countdown(n): #Rekursionsbeispiel
    if n > 0:
        n-=1
        print(n)
        recursive_countdown(n)

def countdown(count):
    while count>=0:
        print(count)
        count-=1
        time.sleep(1)

def countdownusedtoletthingsrise(count):
    while count>=0:
        print(" ")
        count-=1
        time.sleep(1)

def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext



def print_progress(total, index, showed):
    
    perc = index/total*100
    if perc >= 10 and 10 not in showed:
        showed.append(10)
        print("Finished 10%")
    elif perc >= 20 and 20 not in showed:
        showed.append(20)
        print("Finished 20%")
    elif perc >= 30 and 30 not in showed:
        showed.append(30)
        print("Finished 30%")
    elif perc >= 40 and 40 not in showed:
        showed.append(40)
        print("Finished 40%")
    elif perc >= 50 and 50 not in showed:
        showed.append(50)
        print("Finished 50%")
    elif perc >= 60 and 60 not in showed:
        showed.append(60)
        print("Finished 60%")
    elif perc >= 70 and 70 not in showed:
        showed.append(70)
        print("Finished 70%")
    elif perc >= 80 and 80 not in showed:
        showed.append(80)
        print("Finished 80%")
    elif perc >= 90 and 90 not in showed:
        showed.append(90)
        print("Finished 90%")
    
    
    return showed



def bulk_rename(folder_path, start, stop=0):
    """needs os imported"""
    #from os import listdir, rename
    for file in os.listdir(folder_path):
        print("File:\n", file)
        print("After:\n", file[start:len(file)-stop])
        new_name=file[start:len(file)-stop]
        x=input("Happy with that y/N?")
        old_path = folder_path+"/"+file
        new_path = folder_path+"/"+new_name
        if x == "y":
            os.rename(old_path, new_path)
#bulk_rename("/home/dranokami/Documents/music/Game OST/DragonQuestMonster/", 34,16)

#from os import listdir  
def collect_folder_contents(directory):
    return os.listdir(directory)

#from os import path, mkdir
def file_exist(filepath):
    return os.path.isfile(filepath)

def path_exist(mypath):
    return os.path.isdir(mypath)

def make_path(mypath):
    os.mkdir(mypath)

def sum_of_first_row(content, header, sep):
    rows = content.split("\n")
    rows = rows[header:] #cut header
    nums = []
    for row in rows:
        first_col = row.split(sep)[0].replace(",", ".")
        first_col = float(first_col)
        nums.append(first_col)
    print(sum(nums))
    return sum(nums)

def calc_expenses(path):
    fun = read(path+"ausgaben_fun")
    pflicht = read(path+"ausgaben_pflicht")

    funTTL = sum_of_first_row(fun, 2, " ")
    pflichtTTL = sum_of_first_row(pflicht, 2, " ")

    write(path+"ausgaben_fun", fun+"\nTTL: "+str(funTTL))
    write(path+"ausgaben_pflicht", pflicht+"\nTTL: "+str(pflichtTTL))
    #write(path+"aoifsaoif", "hi")


#MODULES SECTION!

############################REGEX################################
#regex tutorial
#re kriegt ein r vor den string, dann werden slashes anders behandelt,
#vordefinierte gruppen wie \d und \w (digit/word) funktionieren dann
#es wird dann als rawstring (daher "r") interpretiert, sodass \n kein newline ist
#unicode rawstrings= ur'a-zäöü'

def basic_ex(pattern, string):
    match= re.match(pattern, string)
    print(match.group())
    #use group(1), group(2) etc to find multiple matches. an index safer way of doing so is shown in regex_find_multiple_example()


def regex_find_multiple_matches_example(pattern, string):
#import re
    compiled_pattern = re.compile(pattern)
    res = re.finditer(compiled_pattern, string)
    return res 
    #res can then be iterated over, they are not the strings itself but match objects!
    #if you just want the strings, use regex_find_multiple_strings_example()

def regex_find_multiple_strings_example(pattern, string):
#import re
    compiled_pattern = re.compile(pattern)
    res = re.findall(compiled_pattern, string)
    return res 

def regex_remove_example(pattern,string):
#import re
    compiled_pattern = re.compile(pattern)
    res = re.sub(compiled_pattern, "", string)
    return res

def regex_find_url_html(string):
    pattern=r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)"
    return re.match(pattern, string)
    #some test objects:
    #link = "www.google.de"
    #link2 = "http://www.esa.int/identity/01L_color_03.html"
    #link3 = "http://ufa.esac.esa.int/ufa/#home"
    #mail =  "david@gmail.com"
    #mail2 = "a.balogh@imperial.ac.uk"
    #l = [link, link2,link3, mail, mail2]
    #for case in l:
    #print("true") if is_link(case) else print("false")

##########################DATETIME##################################
import datetime 
  
today = datetime.datetime.today() 
#x = today:%B does NOT work, but when used in formating:
print(f"{today:%B}")
"""
%b: Returns the first three characters of the month name, like "Sep"
%d: Returns day of the month
%Y: Returns the year in four-digit format
%H: Returns the hour
%M: Returns the minute
%S: Returns the second,"""
##########################LOGGING##################################
def logger():
    # import logging
    
    logging.basicConfig(level=logging.DEBUG, 
                        format=' %(asctime)s - %(levelname)s - %(message)s')
    
    logging.debug('Some debugging details.')

    #to log to a file:
    logging.basicConfig(filename='myLog.txt', 
        level=logging.DEBUG, 
        format='%(asctime)s - %(levelname)s - %(message)s')

##########################PANDAS####################################
#needs import pandas as pd
def read_csv(path2file):
    return pd.read_csv("path2file", sep=";")

def get_columns(some_df):
    some_df=some_df[['SomeColumn','SomeOtherColumn']]

def get_set_of_column(some_df):
    return set(some_df["column"]) #can be replaced with list

def df_from_list(list_of_lists): 
    pd.DataFrame(list(zip(list_of_lists)),
              columns=['lst1_title','lst2_title', 'lst3_title'])

#########################WORDNET#####################################
# all funcs require from nltk.corpus import wordnet
def get_syns(word):
    return wordnet.synsets(word)

def get_path_length(word1, word2):
    #takes 2 syns
    return word1.shortest_path_distance(word2)

def WUP(word1, word2):
    #Wu Palmer: 
    #(2x path from LCS to root) 
    #--------------------------
    #(path from w1 to LCS + path from w2 to LCH + 2x path from LCS to root)
    return word1.lch_similarity(word2)

def LCH(word1, word2):
    # Leacock Chodorow: 
    #-log (path from w1 to w2)
    return word1.wup_similarity(word2)


############################## WORKING DIRECTORY #######################
#from os import getcwd, path, chdir

#one can call the python main from everywhere, the working directory will be
#the one from where it was called. to see it, do:
os.getcwd()

#to find out where the main actually is, do:
os.path.dirname(__file__)

#you can use this info to set the working directory to the main:
#now, all your harcoded-path statements will run, no matter from where the main is called
#WARNING: this might be bad practice, use with discretion 
os.chdir(os.path.dirname(__file__))


############################## VIRTUAL ENVIRONMENTS  ###########################
#are used to seperate your used modules. it's a good idea to keep all your pip installs
#seperated from each other

#to start a virtualenv, do: 
#python3 -m venv <environment-name>

#you can then go into it with:
#source ./env/bin/activate

#and go out with:
#deactivate

#for python 2 virtualEnvs, do virtualenv -p /usr/bin/python2 <envname> 


############################## ANACONDA & JUPYTER NOTEBOOK ############################
#if you installed python via anaconda, you might suddenly see a "base" in front of your terminal prompt
#to hide it, do: conda config --set changeps1 False
#undo within current cell: CTRL-Z
#undo outside curent cell: ESC-Z (un-deletes cells) 


if __name__ == "__main__":
    0
    #calc_expenses("/home/dranokami/Documents/notes/")
    #bulk_rename("../../music/I Build The Sky/2014 - Intortus EP", 45)
    #list_like_commons(dummy_tuple)
    #get_example_counting()
    #lambda_examples()