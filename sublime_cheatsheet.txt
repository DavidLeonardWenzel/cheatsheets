All  commands assume no mac keyboard and have been tested on a german keyboard
If you set Visual Studio Code to behave like sublime, almost all commands work the same.


select also the next instance of variable the cursors at:
CTRL D
try me try me

select also everything selected: 
CTRL SHIFT L 

to move selected lines smart:
CTRL+SHIFT+ARROW KEYS

show/hide Sidebar:
CTRL K, then B
For Visual Studio Code:
CTRL + B

switch to sidebar/back (in order control sidebar with arrow keys)
CTRL + 0/1

go to any file in current projekt:
CTRL + P (then type name)

set syntax:
CTRL+SHIFT+L

build:
CTRL+B

gotoline:
CTRL+G (then type number)

delete line / remove line / kill line:
CTRL+SHIFT+K


soft undo (get back to last cursor position!)
CTRL+U


----------------------------- BUILDING -------------------------------

for building different python envs. instead of the regular, do:
(only tested with anaconda)
Tools->BuildSystem -> New build System, put this here:
{
"cmd": ["/path/to/anaconda3/for/example/python3", "-u", "$file"],
"file_regex": "^[ ]*File \"(...*?)\", line ([0-9]*)",
"selector": "source.python"
}
safe under a name of your choice, then build with CTRL+SHIFT+B 

building tex with sublime:
Option 1:) use latexTools
https://latextools.readthedocs.io/en/latest/

Option 2: ) 

- install texlive (under arch: sudo pacman -S textlive-most)
- open terminal, type "which pdflatex", this gives you the path (for eg: /usr/bin/pdflatex)
- Do Tools > Build System -> New Build System

copy the following stuff: 
{
    "cmd": ["/your/path/here","$file"],
    "selector": "text.tex.latex"
}


{
    "cmd": ["/usr/bin/pdflatex","$file"],
    "selector": "text.tex.latex"
}


To have lines moving like in eclipse+visual studio, go to Preferences, Keybindings and add:

[
	{ "keys": ["alt+up"], "command": "swap_line_up" },
	{ "keys": ["alt+down"], "command": "swap_line_down" }
]
